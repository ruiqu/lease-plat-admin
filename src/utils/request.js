import axios from "axios";
import { Notification, MessageBox, Message } from "element-ui";
import store from "@/store";
import { getToken } from "@/utils/auth";
import errorCode from "@/utils/errorCode";
import Vue from "vue";

axios.defaults.headers["Content-Type"] = "application/json;charset=utf-8";
// 创建axios实例
const service = axios.create({
  // axios中请求配置有baseURL选项，表示请求URL公共部分
  baseURL: process.env.VUE_APP_BASE_API,
  // 超时
  timeout: 1000000,
});
// request拦截器
service.interceptors.request.use(
  (config) => {
    // 是否需要设置 token
    const isToken = (config.headers || {}).isToken === false;
    if (getToken() && !isToken) {
      config.headers["Authorization"] = "Bearer " + getToken(); // 让每个请求携带自定义token 请根据实际情况自行修改
    }
    // get请求映射params参数
    if (config.data && !config.custom) {
      let url = config.url + "?";
      if (config.isArr) {
        for (const propName of Object.keys(config.data)) {
          const value = config.data[propName];
          var part = propName + "=";
          if (value !== null && typeof value !== "undefined") {
            url += part + value + "&";
          }
        }
      } else {
        for (const propName of Object.keys(config.data)) {
          const value = config.data[propName];
          var part = propName + "=";
          if (value !== null && typeof value !== "undefined") {
            if (typeof value === "object") {
              for (const key of Object.keys(value)) {
                let params = propName + "[" + key + "]";
                var subPart = params + "=";
                url += subPart + value[key] + "&";
              }
            } else {
              url += part + value + "&";
            }
          }
        }
      }

      url = url.slice(0, -1);
      config.params = {};
      config.url = url;
    }
    return config;
  },
  (error) => {
    console.log(error);
    Promise.reject(error);
  }
);

// 响应拦截器
service.interceptors.response.use(
  (res) => {
    // 未设置状态码则默认成功状态
    const code = res.data.code || 200;
    // 获取错误信息
    console.log(res.data.message, errorCode[code], "--->res.data.message");
    const msg = res.data.message || errorCode[code] || errorCode["default"];
    if (code === 401) {
      if (!window.location.href.includes("#/login")) {
        MessageBox.confirm("登录状态已过期，您可以继续留在该页面，或者重新登录", "系统提示", {
          confirmButtonText: "重新登录",
          cancelButtonText: "取消",
          type: "warning",
        }).then(() => {
          console.log(window.location.href);
          localStorage.clear();
          store.dispatch("LogOut").then(() => {
            store.dispatch("tagsView/delAllViews");

            location.href = window.location.origin + "/admin/index.html#/login";
          });
        });
      } else {
        MessageBox.confirm("登录状态已过期，您可以继续留在该页面，或者重新登录", "系统提示", {
          confirmButtonText: "重新登录",
          cancelButtonText: "取消",
          type: "warning",
        }).then(() => {
          localStorage.clear();
          console.log(window.location.href);
          // store.dispatch("LogOut").then(() => {
          //   location.href = window.location.origin + "/mng/index.html#/login";
          // });
        });
      }
    } else if (code === 500) {
      Message({
        message: msg,
        type: "error",
      });
      return Promise.reject(new Error(msg));
    } else if (code !== 200) {
      Notification.error({
        title: msg,
      });
      return Promise.reject("error");
    } else {
      return res.data;
    }
  },
  (error) => {
    console.log("err" + error);
    let { message } = error;
    if (message == "Network Error") {
      message = "后端接口连接异常";
    } else if (message.includes("timeout")) {
      message = "系统接口请求超时";
    } else if (message.includes("Request failed with status code")) {
      message = "系统接口" + message.substr(message.length - 3) + "异常";
    }
    Message({
      message: message,
      type: "error",
      duration: 5 * 1000,
    });
    return Promise.reject(error);
  }
);

export default service;
