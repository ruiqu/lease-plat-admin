import Cookies from "js-cookie";

const TokenKey = "syrjToken";

export function getToken() {
  return localStorage.getItem(TokenKey);
}

export function setToken(token) {
  localStorage.setItem(TokenKey, token);
}

export function removeToken() {
  Cookies.remove(TokenKey);
}
