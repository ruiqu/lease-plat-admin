import request from "@/utils/request";

/**
 *获取列表
 */
export function suggestionListSvr(data) {
  return request({
    url: "/suggestion/list",
    method: "get",
    data,
  });
}

/**
 * 修改状态
 * @param status 状态：0-待处理；1-已处理
 * @param id
 */
export function updateByIdSuggestionSvr(data) {
  return request({
    url: "/suggestion/updateById",
    method: "post",
    data,
  });
}
