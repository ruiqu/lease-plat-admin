import request from "@/utils/request";

/**
 *获取列表
 * @param {*} data
 * @param idNo 身份证号
 * @param identifyStatus 实名认证状态 0-未认证，1-已通过，2-未通过
 * @param phone 手机号
 * @param userName 姓名模糊关键字
 * @returns
 */
export function userListSvr(data) {
  return request({
    url: "/user/list",
    method: "get",
    data,
  });
}

/**
 * 给用户发放额外奖励
 * @param {*} data
 * @param amount
 * @param remarks
 * @param userId
 * @returns
 */
export function addExtraRewardSvr(data) {
  return request({
    url: "/user/addExtraReward",
    method: "post",
    data,
  });
}

/**
 * 用户提现列表获取列表
 * @param {*} data
 * @param idNo 身份证号
 * @param identifyStatus 实名认证状态 0-未认证，1-已通过，2-未通过
 * @param phone 手机号
 * @param userName 姓名模糊关键字
 * @returns
 */
export function cashoutAuditListSvr(data) {
  return request({
    url: "/cashoutAudit/list",
    method: "get",
    data,
  });
}
// 审核提现
export function auditCashoutAuditSvr(data) {
  return request({
    url: "/cashoutAudit/audit",
    method: "post",
    data,
  });
}
export function getUserInfoByIdSvr(data) {
  return request({
    url: "/user/infoById",
    method: "get",
    data,
  });
}

export function userCashoutlistSvr(data) {
  return request({
    url: "/user/userCashoutlist",
    method: "get",
    data,
  });
}
export function userIncomeslistSvr(data) {
  return request({
    url: "/user/userIncomeslist",
    method: "get",
    data,
  });
}
