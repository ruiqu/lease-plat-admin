import request from "@/utils/request";

// 获取列表
export function adminList(data) {
  return request({
    url: "/admin/list",
    method: "get",
    data,
  });
}
// 新增
export function adminRegister(data) {
  return request({
    url: "/admin/register",
    method: "post",
    data,
  });
}
// 获取制定用户的信息
export function adminInfoById(data) {
  return request({
    url: "/admin/infoById",
    method: "get",
    data,
  });
}

// 修改指定用户的信息
export function adminUpdateById(data) {
  return request({
    url: "/admin/updateById",
    method: "post",
    data,
  });
}
// 删除指定用户的信息
export function adminDeleteById(data) {
  return request({
    url: "/admin/deleteById",
    method: "post",
    data,
  });
}
// 修改指定用户的状态
export function adminUpdateStatusById(data) {
  return request({
    url: "/admin/updateStatusById",
    method: "post",
    data,
  });
}
// 指派角色
export function adminRoleUpdate(data) {
  return request({
    url: "/admin/role/update",
    method: "post",
    data,
  });
}

// 指派角色
export function addminRoleById(data) {
  return request({
    url: "/admin/roleById",
    method: "get",
    data,
  });
}
// 修改制定用户密码
export function adminUpdatePassword(data) {
  return request({
    url: "/admin/updatePassword",
    method: "post",
    data,
  });
}
