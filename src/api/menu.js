import request from "@/utils/request";

// 获取路由
export const getRouters = data => {
  return request({
    url: "/menu/treeList",
    method: "get",
    data
  });
};
// 新增才带你
export const menuCreate = data => {
  return request({
    url: "/menu/create",
    method: "post",
    data
  });
};
// 新增
export const menuDeleteById = data => {
  return request({
    url: "/menu/deleteById",
    method: "post",
    data
  });
};
// 新增xiugai
export const menuUpdate = data => {
  return request({
    url: "/menu/update",
    method: "post",
    data
  });
};
// 改变隐藏状态
export const menuUpdateHiddenById = data => {
  return request({
    url: "/menu/updateHiddenById",
    method: "post",
    data
  });
};
// 改变隐藏状态
export const menuInfoById = data => {
  return request({
    url: "/menu/infoById",
    method: "get",
    data
  });
};
export const menuListByParentId = data => {
  return request({
    url: "/menu/listByParentId",
    method: "get",
    data
  });
};
