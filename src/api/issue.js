import request from "@/utils/request";

// 列表
export function qaList(data) {
  return request({
    url: "/qa/list",
    method: "get",
    data,
  });
}
// 新增
export function createQa(data) {
  return request({
    url: "/qa/create",
    method: "post",
    data,
    custom: true,
  });
}
// 修改
export function qaUpdateById(data) {
  return request({
    url: "/qa/updateById",
    method: "post",
    data,
    custom: true,
  });
}

// 查询详情
export function qaInfoById(data) {
  return request({
    url: "/qa/infoById",
    method: "get",
    data,
  });
}
// 删除
export function deleteByIdQa(data) {
  return request({
    url: "/qa/deleteById",
    method: "post",
    data,
  });
}
