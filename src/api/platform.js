import request from "@/utils/request";

/**
 *获取列表
 * @param {*} data
 * @param categoryId 所属分类ID
 * @param showSts 是否显示：1-是，2-否
 * @param title 名称
 * @returns
 */
export function platformListSvr(data) {
  return request({
    url: "/platform/list",
    method: "get",
    data,
  });
}

/**
 * 新增平台
 * @param {*} data
 * @param "cashbackAmount": 0,
 * @param "categoryId": 0,分类id
 * @param "contacter": "string",联系人
 * @param "contacterPhone": "string",联系电话
 * @param "imageUrl": "string", 封面图
 * @param "introduction": "string", 介绍
 * @param "kickbackDesc": "string",
 * @param "linkQrImg": "string",  跳转二维码
 * @param "linkUrl": "string", 跳转地址
 * @param "reamarks": "string", 备注
 * @param "showSts": 0, 是否显示
 * @param "sortIndex": 0, 排序
 * @param "tags": "string", 标签
 * @param "title": "string",标题
 * @param "todayRentCnt": 0,今日新增
 * @returns
 */
export function createPlatformSvr(data) {
  return request({
    url: "/platform/create",
    method: "post",
    data,
  });
}

/**
 *获取xiangiqng
 * @param {*} data
 * @param id
 * @returns
 */
export function platformInfoByIdSvr(data) {
  return request({
    url: "/platform/infoById",
    method: "get",
    data,
  });
}

// 编辑
export function updateByIdPlatformSvr(data) {
  return request({
    url: "/platform/updateById",
    method: "post",
    data,
    custom: true,
  });
}
// 删除
export function deleteByIdPlatformSvr(data) {
  return request({
    url: "/platform/deleteById",
    method: "post",
    data,
  });
}
