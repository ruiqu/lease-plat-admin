import request from "@/utils/request";

// 列表
export function bannerList(data) {
  return request({
    url: "/banner/list",
    method: "get",
    data
  });
}
// 新增
export function createBanner(data) {
  return request({
    url: "/banner/create",
    method: "post",
    data,
    custom: true
  });
}
// 修改
export function bannerUpdateById(data) {
  return request({
    url: "/banner/updateById",
    method: "post",
    data,
    custom: true
  });
}

// 查询详情
export function bannerInfoById(data) {
  return request({
    url: "/banner/infoById",
    method: "get",
    data
  });
}
// 删除
export function deleteByIdBanner(data) {
  return request({
    url: "/banner/deleteById",
    method: "post",
    data
  });
}
