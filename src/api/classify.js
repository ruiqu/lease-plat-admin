import request from "@/utils/request";

/**
 *获取列表
 * @param {*} data
 * @param showSts 是否显示：1-是，2-否
 * @param title 名称
 * @returns
 */
export function platformCategoryListSvr(data) {
  return request({
    url: "/platformCategory/list",
    method: "get",
    data,
  });
}

/**
 * 新增
 * @param {*} data
 * @param "title": 0,
 * @param "showSts": 0,分类id
 * @param "id": "string",联系人
 * @param "sortIndex": "string",
 * @returns
 */
export function createPlatformCategorySvr(data) {
  return request({
    url: "/platformCategory/create",
    method: "post",
    data,
  });
}

/**
 *  获取详情
 * @param {*} data
 * @param "id": "string",联系人
 * @returns
 */
export function infoByIdPlatformCategorySvr(data) {
  return request({
    url: "/platformCategory/infoById",
    method: "get",
    data,
  });
}
/**
 * 新增
 * @param {*} data
 * @param "title": 0,
 * @param "showSts": 0,分类id
 * @param "id": "string",联系人
 * @param "sortIndex": "string",
 * @returns
 */
export function updateByIdPlatformCategorySvr(data) {
  return request({
    url: "/platformCategory/updateById",
    method: "post",
    data,
  });
}

/**
 *获取列表 不分页
 * @returns
 */
export function allShowListNoPaggingSvr(data) {
  return request({
    url: "/platformCategory/allShowListNoPagging",
    method: "get",
    data,
  });
}

/**
 * 删除入口
 * @param {*} data
 * @param id
 * @returns
 */
export function deleteByIdplatformCategorySvr(data) {
  return request({
    url: "/platformCategory/deleteById",
    method: "post",
    data,
  });
}
