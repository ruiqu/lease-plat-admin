import request from "@/utils/request";

// 列表
export function channelList(data) {
  return request({
    url: "/channel/list",
    method: "get",
    data,
  });
}
// 新增
export function createChannel(data) {
  return request({
    url: "/channel/create",
    method: "post",
    data,
    custom: true,
  });
}
// 修改
export function channelUpdateById(data) {
  return request({
    url: "/channel/updateById",
    method: "post",
    data,
    custom: true,
  });
}

// 查询详情
export function channelInfoById(data) {
  return request({
    url: "/channel/infoById",
    method: "get",
    data,
  });
}
// 删除
export function deleteByIdchannel(data) {
  return request({
    url: "/channel/deleteById",
    method: "post",
    data,
  });
}

// 渠道订单
export function channelAdminOrderSvr(data) {
  return request({
    url: "/channelAdmin/orderList",
    method: "get",
    data,
  });
}
// 渠道订单
export function channelAdminInfoSvr(data) {
  return request({
    url: "/channelAdmin/info",
    method: "get",
    data,
  });
}
