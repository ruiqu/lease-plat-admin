import request from "@/utils/request";

// 列表
export function settingList(data) {
  return request({
    url: "/setting/list",
    method: "get",
    data,
  });
}
// 修改
export function settingUpdateById(data) {
  return request({
    url: "/setting/updateById",
    method: "post",
    data,
    custom: true,
  });
}

// 查询详情
export function settingInfoById(data) {
  return request({
    url: "/setting/infoById",
    method: "get",
    data,
  });
}
