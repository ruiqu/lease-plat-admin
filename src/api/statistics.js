import request from "@/utils/request";
//获取平台
export function statisticsupsGetList(data) {
  return request({
    url: "/channel/vindicate/getList",
    method: "get",
    data,
  });
}

//上游渠道管理列表
export function statisticsupsList(data) {
    return request({
      url: "/channel/vindicate/list",
      method: "get",
      data,
    });
  }


  //删除上游渠道
  export function statisticsdel(data) {
    return request({
      url: "/channel/vindicate/del",
      method: "get",
      data,
    });
  }
  
  //新增上游渠道
  export function statisticsadd(data) {
    return request({
      url: "/channel/vindicate/add",
      method: "post",
      data,
    });
  }

  //更新渠道
  export function statisticsupdate(data) {
    return request({
      url: "/channel/vindicate/update",
      method: "post",
      data,
    });
  }
  
//查询渠道明细
  export function statisticsdetail(data) {
    return request({
      url: "/channel/vindicate/detail",
      method: "get",
      data,
    });
  }

  //更新渠道状态
  export function statisticschangeState(data) {
    return request({
      url: "/channel/vindicate/changeState",
      method: "get",
      data,
    });
  }


  
//上游渠道统计列表
export function statisticsupList(data) {
  return request({
    url: "/channel/stat/list",
    method: "get",
    data,
  });
}


//产品渠道列表
export function platformList(data) {
  return request({
    url: "/platform/stat/list",
    method: "get",
    data,
  });
}







  // 总统计列表
export function getAll(data) {
  return request({
    url: "/channel/stat/all/list",
    method: "get",
    data,
  });
}
  
  


  



  
