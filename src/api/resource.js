import request from "@/utils/request";

// 新增
export function resourceCreate(data) {
  return request({
    url: "/resource/create",
    method: "post",
    data
  });
}
// 获取列表
export function resourceList(data) {
  return request({
    url: "/resource/list",
    method: "get",
    data
  });
}
// 获取所有资源类标
export function resourceListAll(data) {
  return request({
    url: "/resource/listAll",
    method: "get",
    data
  });
}
// 更新资源列表
export function resourceUpdateById(data) {
  return request({
    url: "/resource/updateById",
    method: "post",
    data
  });
}

// 删除资源列表
export function resourceDeleteById(data) {
  return request({
    url: "/resource/deleteById",
    method: "post",
    data
  });
}
// 查询详情
export function resourceInfoById(data) {
  return request({
    url: "/resource/infoById",
    method: "post",
    data
  });
}
