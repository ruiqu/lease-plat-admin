import request from "@/utils/request";

// 列表
export function manualList(data) {
  return request({
    url: "/manual/list",
    method: "get",
    data,
  });
}

// 修改
export function manualUpdateById(data) {
  return request({
    url: "/manual/updateById",
    method: "post",
    data,
    custom: true,
  });
}

// 查询详情
export function manualInfoById(data) {
  return request({
    url: "/manual/infoById",
    method: "get",
    data,
  });
}
