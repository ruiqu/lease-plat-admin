import request from "@/utils/request";

// 获取药厂列表
export function ruleList(data) {
  console.log(data, "----------->", data);
  return request({
    url: "/role/list",
    method: "get",
    data,
  });
}

// 新增
export function roleCreate(data) {
  return request({
    url: "/role/create",
    method: "post",
    data,
  });
}
// 更新订单
export function roleUpdateById(data) {
  return request({
    url: "/role/updateById",
    method: "post",
    data,
  });
}
// 更新角色显示状态
export function roleUpdateStatusById(data) {
  return request({
    url: "/role/updateStatusById",
    method: "post",
    data,
    // custom: true
  });
}
// 删除
export function roleDelete(data) {
  return request({
    url: "/role/delete",
    method: "post",
    data,
    // custom: true
  });
}
// 给角色分配菜单
/**
 *
 * @param  menuIds data
 * @param   roleId
 */
export function roleAllocMenuByRoleId(data) {
  return request({
    url: "/role/allocMenuByRoleId",
    method: "post",
    data,
    // custom: true
  });
}

/**
 * 查询所有角色
 */
export function roleListAll(data) {
  return request({
    url: "/role/listAll",
    method: "get",
    data,
    // custom: true
  });
}
/**
 * 获取角色的所有菜单
 */
export function roleListMenuByRoleId(data) {
  return request({
    url: "/role/listMenuByRoleId",
    method: "get",
    data,
    // custom: true
  });
}
/**
 * 获取角色的相关资源
 */
export function listResourceByRoleId(data) {
  return request({
    url: "/role/listResourceByRoleId",
    method: "get",
    data,
    // custom: true
  });
}
/**
 * 给角色分配资源
 */
export function allocResourceByRoleId(data) {
  return request({
    url: "/role/allocResourceByRoleId",
    method: "post",
    data,
    isArr: true,
  });
}
