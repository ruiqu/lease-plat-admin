import request from "@/utils/request";

/**
 *获取列表
 * @param {*} data
 * @param showSts 是否显示：1-是，2-否
 * @param title 名称
 * @returns
 */
export function platformEntryListSvr(data) {
  return request({
    url: "/platformEntry/list",
    method: "get",
    data,
  });
}

/**
 * 新增入口
 * @param {*} data
 * @param title
 * @param sortIndex
 * @param showSts
 * @returns
 */
export function createplatformEntrySvr(data) {
  return request({
    url: "/platformEntry/create",
    method: "post",
    data,
  });
}
/**
 * 删除入口
 * @param {*} data
 * @param id
 * @returns
 */
export function deleteByIdplatformEntrySvr(data) {
  return request({
    url: "/platformEntry/deleteById",
    method: "post",
    data,
  });
}

/**
 * 获取详情
 * @param {*} data
 * @param id
 * @returns
 */
export function infoByIdplatformEntryListSvr(data) {
  return request({
    url: "/platformEntry/infoById",
    method: "get",
    data,
  });
}
/**
 * 编辑
 * @param {*} data
 * @returns
 */
export function updateByIdplatformEntrySvr(data) {
  return request({
    url: "/platformEntry/updateById",
    method: "post",
    data,
  });
}

/**
 * 获取入口下的平台
 * @param id
 * @param title
 *
 */
export function getPlatformListByEntryIdSvr(data) {
  return request({
    url: "/platformEntry/getPlatformListByEntryId",
    method: "get",
    data,
  });
}

/**
 * 给平台添加标签
 * @param id
 * @param title
 *
 */
export function createEntryMappingSvr(data) {
  return request({
    url: "/platformEntry/createEntryMapping",
    method: "post",
    data,
  });
}
/**
 * 移除标签下的平台
 * @param id
 *
 */
export function deleteEntryMappingByIdSvr(data) {
  return request({
    url: "/platformEntry/deleteEntryMappingById",
    method: "post",
    data,
  });
}
/**
 * 移动顺序
 * @param id，
 * labelId
 * sortIndex
 * platformId
 *
 */
export function updateEntryMappingByIdSvr(data) {
  return request({
    url: "/platformEntry/updateEntryMappingById",
    method: "post",
    data,
  });
}
