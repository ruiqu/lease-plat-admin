import request from "@/utils/request";

// 列表
export function newsList(data) {
  return request({
    url: "/news/list",
    method: "get",
    data,
  });
}
// 新增
export function createNews(data) {
  return request({
    url: "/news/create",
    method: "post",
    data,
    custom: true,
  });
}
// 修改
export function newsUpdateById(data) {
  return request({
    url: "/news/updateById",
    method: "post",
    data,
    custom: true,
  });
}

// 查询详情
export function newsInfoById(data) {
  return request({
    url: "/news/infoById",
    method: "get",
    data,
  });
}
// 删除
export function deleteByIdnews(data) {
  return request({
    url: "/news/deleteById",
    method: "post",
    data,
  });
}
