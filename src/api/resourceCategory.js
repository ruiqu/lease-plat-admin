import request from "@/utils/request";

// 获取列表
export function resourceCategoryCreate(data) {
  return request({
    url: "/resourceCategory/create",
    method: "post",
    data
  });
}
// 新增
export function resourceCategoryListAll(data) {
  return request({
    url: "/resourceCategory/listAll",
    method: "get",
    data
  });
}
// 更新
export function resourceCategoryUpdateById(data) {
  return request({
    url: "/resourceCategory/updateById",
    method: "post",
    data
  });
}

// 删除
export function resourceCategoryDeleteById(data) {
  return request({
    url: "/resourceCategory/deleteById",
    method: "post",
    data
  });
}
