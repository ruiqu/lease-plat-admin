import request from "@/utils/request";
// 列表
export function qaCategoryList(data) {
  return request({
    url: "/qaCategory/list",
    method: "get",
    data,
  });
}
// 新增
export function createQaCategory(data) {
  return request({
    url: "/qaCategory/create",
    method: "post",
    data,
    custom: true,
  });
}
// 修改
export function qaCategoryUpdateById(data) {
  return request({
    url: "/qaCategory/updateById",
    method: "post",
    data,
    custom: true,
  });
}

// 查询详情
export function qaCategoryInfoById(data) {
  return request({
    url: "/qaCategory/infoById",
    method: "get",
    data,
  });
}
// 删除
export function deleteByIdQaCategory(data) {
  return request({
    url: "/qaCategory/deleteById",
    method: "post",
    data,
  });
}
// 列表
export function qaCategoryAllShowListNoPagging(data) {
  return request({
    url: "/qaCategory/allShowListNoPagging",
    method: "get",
    data,
  });
}
