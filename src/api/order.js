import request from "@/utils/request";

export const orderListSvr = (data) => {
  return request({
    url: "/order/list",
    method: "get",
    data,
  });
};
export const orderInfoByIdSvr = (data) => {
  return request({
    url: "/order/infoById",
    method: "get",
    data,
  });
};

export const orderExportListSvr = (data) => {
  return request({
    url: "/order/exportList",
    method: "get",
    data,
  });
};

export const changeOrderStatusSvr = (data) => {
  return request({
    url: "/order/changeOrderStatus",
    method: "post",
    data,
  });
};
