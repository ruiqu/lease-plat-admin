import request from "@/utils/request";

// 获取药厂列表
export function versionInfo(data) {
  //   console.log(data, "----------->", data);
  return request({
    url: "/version/info",
    method: "get",
    data
  });
}

// 新增
export function versionUpdateById(data) {
  return request({
    url: "/version/updateById",
    method: "post",
    data
  });
}
