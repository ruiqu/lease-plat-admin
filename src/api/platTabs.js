import request from "@/utils/request";

/**
 *获取列表
 * @param {*} data
 * @param showSts 是否显示：1-是，2-否
 * @param title 名称
 * @returns
 */
export function platformLabelListSvr(data) {
  return request({
    url: "/platformLabel/list",
    method: "get",
    data,
  });
}

/**
 * 新增标签
 * @param {*} data
 * @param title
 * @param sortIndex
 * @param showSts
 * @returns
 */
export function createPlatformLabelSvr(data) {
  return request({
    url: "/platformLabel/create",
    method: "post",
    data,
  });
}
/**
 * 删除标签
 * @param {*} data
 * @param id
 * @returns
 */
export function deleteByIdPlatformLabelSvr(data) {
  return request({
    url: "/platformLabel/deleteById",
    method: "post",
    data,
  });
}

/**
 * 获取详情
 * @param {*} data
 * @param id
 * @returns
 */
export function infoByIdPlatformLabelListSvr(data) {
  return request({
    url: "/platformLabel/infoById",
    method: "get",
    data,
  });
}
/**
 * 编辑
 * @param {*} data
 * @returns
 */
export function updateByIdPlatformLabelSvr(data) {
  return request({
    url: "/platformLabel/updateById",
    method: "post",
    data,
  });
}

/**
 * 获取标签下的平台
 * @param id
 * @param title
 *
 */
export function getPlatformListByLabelIdSvr(data) {
  return request({
    url: "/platformLabel/getPlatformListByLabelId",
    method: "get",
    data,
  });
}
/**
 * 给平台添加标签
 * @param id
 * @param title
 *
 */
export function createLabelMappingSvr(data) {
  return request({
    url: "/platformLabel/createLabelMapping",
    method: "post",
    data,
  });
}
/**
 * 移除标签下的平台
 * @param id
 *
 */
export function deleteLabelMappingByIdSvr(data) {
  return request({
    url: "/platformLabel/deleteLabelMappingById",
    method: "post",
    data,
  });
}
/**
 * 移动顺序
 * @param id，
 * labelId
 * sortIndex
 * platformId
 *
 */
export function updateLabelMappingByIdSvr(data) {
  return request({
    url: "/platformLabel/updateEntryMappingById",
    method: "post",
    data,
  });
}
