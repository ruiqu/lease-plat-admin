export const tableColumns = [
  {
    label: "设置项key",
    align: "center",
    prop: "settingKey",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "设置项值",
    align: "center",
    prop: "settingVal",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "说明",
    align: "center",
    prop: "remarks",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "操作",
    align: "center",
    prop: "operation",
    width: 120,
  },
];
