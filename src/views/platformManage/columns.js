export const tableColumns = [
  {
    label: "logo",
    align: "center",
    prop: "imageUrl",
  },
  {
    label: "平台名称",
    align: "center",
    prop: "title",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "创建时间",
    align: "center",
    prop: "insTime",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "分类",
    align: "center",
    prop: "categoryTitle",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "是否显示",
    align: "center",
    prop: "showSts",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "联系人",
    align: "center",
    prop: "contacter",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "联系号码",
    align: "center",
    prop: "contacterPhone",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "操作",
    align: "center",
    prop: "operation",
    width: 120,
  },
];
export const tableClassifyColumns = [
  {
    label: "logo",
    align: "center",
    prop: "imageUrl",
  },
  {
    label: "平台名称",
    align: "center",
    prop: "title",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "排序",
    align: "center",
    prop: "sortIndex",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "创建时间",
    align: "center",
    prop: "insTime",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "分类",
    align: "center",
    prop: "categoryTitle",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "是否显示",
    align: "center",
    prop: "showSts",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "联系人",
    align: "center",
    prop: "contacter",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "联系号码",
    align: "center",
    prop: "contacterPhone",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "操作",
    align: "center",
    prop: "operation",
    width: 120,
  },
];
export const labelTableColumns = [
  {
    label: "平台名称",
    align: "center",
    prop: "platformTitle",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "排序",
    align: "center",
    prop: "sortIndex",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "操作",
    align: "center",
    prop: "operation",
    width: 120,
  },
];
// platformTitle

export const selectTableColumns = [
  {
    label: "logo",
    align: "center",
    prop: "imageUrl",
    showOverflowTooltip: true,
  },
  {
    label: "平台名称",
    align: "center",
    prop: "title",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "创建时间",
    align: "center",
    prop: "insTime",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "分类",
    align: "center",
    prop: "categoryTitle",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "联系人",
    align: "center",
    prop: "contacter",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "联系号码",
    align: "center",
    prop: "contacterPhone",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "联系号码",
    align: "center",
    prop: "orderNum",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "操作",
    align: "center",
    prop: "operation",
    width: 120,
  },
];
