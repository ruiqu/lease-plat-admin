export const menuTableColumns = [
  {
    label: "封面图",
    align: "center",
    prop: "imageUrl",
    showOverflowTooltip: true,
  },
  {
    label: "入口名称",
    align: "center",
    prop: "title",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "入口id",
    align: "center",
    prop: "id",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "排序",
    align: "center",
    prop: "sortIndex",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "操作",
    align: "center",
    prop: "operation",
    width: 200,
  },
];
