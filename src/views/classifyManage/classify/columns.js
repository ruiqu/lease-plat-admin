export const menuTableColumns = [
  {
    label: "分类名称",
    align: "center",
    prop: "title",
    showOverflowTooltip: true,
  },
  {
    label: "排序",
    align: "center",
    prop: "sortIndex",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "操作",
    align: "center",
    prop: "operation",
    width: 200,
  },
];
