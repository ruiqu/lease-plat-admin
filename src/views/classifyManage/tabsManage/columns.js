export const tableColumns = [
  {
    label: "标签名称",
    align: "center",
    prop: "title",
    showOverflowTooltip: true,
  },
  {
    label: "排序",
    align: "center",
    prop: "sortIndex",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "操作",
    align: "center",
    prop: "operation",
    width: 200,
  },
];
export const tableData = [
  {
    title: "标签1",
    id: "1",
    sort: 999,
  },
  {
    title: "标签2",
    id: "2",
    sort: 999,
  },
  {
    title: "标签3",
    id: "3",
    sort: 999,
  },
];
