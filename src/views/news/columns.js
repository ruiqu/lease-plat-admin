export const tableColumns = [
  {
    label: "主题",
    align: "center",
    prop: "title",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "状态",
    align: "center",
    prop: "showSts",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "排序",
    align: "center",
    prop: "sortIndex",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "操作",
    align: "center",
    prop: "operation",
    width: 120,
  },
];
