export const menuTableColumns = [
  {
    label: "用户编号",
    align: "center",
    prop: "id",
    showOverflowTooltip: true,
  },
  {
    label: "姓名",
    align: "center",
    prop: "userName",
    showOverflowTooltip: true,
  },

  {
    label: "昵称",
    align: "center",
    prop: "nickName",
    showOverflowTooltip: true,
  },
  {
    label: "可提现金额",
    align: "center",
    prop: "surplusAmount",
    showOverflowTooltip: true,
  },

  {
    label: "联系方式",
    align: "center",
    prop: "phone",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "注册时间",
    align: "center",
    prop: "insTime",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "是否实名",
    align: "center",
    prop: "identifyStatus",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "渠道名称",
    align: "center",
    prop: "channelTitle",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "订单数量",
    align: "center",
    prop: "orderCntAll",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "完结数量",
    align: "center",
    prop: "orderCntSettlement",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "操作",
    align: "center",
    prop: "operation",
    width: 200,
  },
];

export const shouruColumns = [
  {
    label: "收益来源类型",
    align: "center",
    prop: "incomeType",
    showOverflowTooltip: true,
  },
  {
    label: "收益金额",
    align: "center",
    prop: "amount",
    showOverflowTooltip: true,
  },
  {
    label: "订单编号",
    align: "center",
    prop: "orderSn",
    showOverflowTooltip: true,
  },
  {
    label: "备注",
    align: "center",
    prop: "remarks",
    showOverflowTooltip: true,
  },

  {
    label: "创建时间",
    align: "center",
    prop: "insTime",
    showOverflowTooltip: true,
  },
];

export const zhichuColumns = [
  {
    label: "提现方式",
    align: "center",
    prop: "type",
    showOverflowTooltip: true,
  },
  {
    label: "提现账户",
    align: "center",
    prop: "withdrawalAccount",
    showOverflowTooltip: true,
  },
  {
    label: "提现金额",
    align: "center",
    prop: "amount",
    showOverflowTooltip: true,
  },
  {
    label: "提现状态",
    align: "center",
    prop: "drawingSts",
    showOverflowTooltip: true,
  },
  {
    label: "提现备注",
    align: "center",
    prop: "remarks",
    showOverflowTooltip: true,
  },
];
