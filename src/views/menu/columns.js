export const menuTableColumns = [
  {
    label: "菜单名称",
    align: "center",
    prop: "title",
    showOverflowTooltip: true,
  },
  {
    label: "是否隐藏",
    align: "center",
    prop: "hidden",
    showOverflowTooltip: true,
  },
  {
    label: "标识",
    align: "center",
    prop: "name",
    showOverflowTooltip: true,
  },
  {
    label: "操作",
    align: "center",
    prop: "operation",
  },
];
