export const tableColumns = [
  {
    label: "主图",
    align: "center",
    prop: "imageUrl",
    showOverflowTooltip: true,
  },
  {
    label: "主题",
    align: "center",
    prop: "title",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "状态",
    align: "center",
    prop: "showSts",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "操作",
    align: "center",
    prop: "operation",
    width: 120,
  },
];
