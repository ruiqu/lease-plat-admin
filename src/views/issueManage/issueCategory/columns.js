export const tableColumns = [
  {
    label: "图片",
    align: "center",
    prop: "imageUrl",
    showOverflowTooltip: true,
    // type: "index",
    width: "330px",
  },
  {
    label: "主题",
    align: "center",
    prop: "title",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "状态",
    align: "center",
    prop: "showSts",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "排序",
    align: "center",
    prop: "sortIndex",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "操作",
    align: "center",
    prop: "operation",
    width: 120,
  },
];
