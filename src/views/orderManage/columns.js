export const menuTableColumns = [
  {
    label: "订单编号",
    align: "center",
    prop: "orderSn",
    showOverflowTooltip: true,
  },
  {
    label: "下单人姓名",
    align: "center",
    prop: "userName",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "下单人手机号",
    align: "center",
    prop: "phone",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "平台名称",
    align: "center",
    prop: "platformTitle",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "渠道名称",
    align: "center",
    prop: "channelTitle",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "下单时间",
    align: "center",
    prop: "insTime",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "订单状态",
    align: "center",
    prop: "status",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "操作",
    align: "center",
    prop: "operation",
    width: 220,
  },
];
