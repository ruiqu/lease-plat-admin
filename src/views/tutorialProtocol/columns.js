export const tableColumns = [
  {
    label: "封面图",
    align: "center",
    prop: "imageUrl",
    showOverflowTooltip: true,
  },
  {
    label: "主题",
    align: "center",
    prop: "title",
    showOverflowTooltip: true,
  },
  // {
  //   label: "状态",
  //   align: "center",
  //   prop: "showSts",
  //   showOverflowTooltip: true,
  // },

  // {
  //   label: "排序",
  //   align: "center",
  //   prop: "sortIndex",
  //   showOverflowTooltip: true,
  // },
  {
    label: "操作",
    align: "center",
    prop: "operation",
    width: 120,
  },
];
