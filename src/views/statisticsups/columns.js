export const tableColumns1 = [
  {
    label: "渠道名称",
    align: "center",
    prop: "channelName",
    showOverflowTooltip: true,
    // type: "index",
  },
  
  {
    label: "分享链接",
    align: "center",
    prop: "jumpUrl",
    width: 230,
    showOverflowTooltip: true,
  },
  // {
  //   label: "外部分享链接",
  //   align: "center",
  //   prop: "outLinkUrl",
  //   width: 230,
  //   showOverflowTooltip: true,
  // },
  {
    label: "联系人",
    align: "center",
    prop: "principal",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "联系人电话",
    align: "center",
    prop: "contactPhone",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "状态",
    align: "center",
    prop: "state",
    showOverflowTooltip: true,
    // type: "index",
  },
  // {
  //   label: "访客数量",
  //   align: "center",
  //   prop: "uvCnt",
  //   showOverflowTooltip: true,
  //   // type: "index",
  // },
  // {
  //   label: "注册数量",
  //   align: "center",
  //   prop: "registerCnt",
  //   showOverflowTooltip: true,
  //   // type: "index",
  // },
  {
    label: "操作",
    align: "center",
    prop: "operation",
    width: 300,
  },
];

export const tableColumns = [
  {
    label: "订单号",
    align: "center",
    prop: "orderSn",
    showOverflowTooltip: true,
  },
  {
    label: "下单人姓名",
    align: "center",
    prop: "userName",
    showOverflowTooltip: true,
  },
  {
    label: "下单人手机号",
    align: "center",
    prop: "phone",
    showOverflowTooltip: true,
  },
  {
    label: "平台名称",
    align: "center",
    prop: "platformTitle",
    showOverflowTooltip: true,
  },
  {
    label: "下单时间",
    align: "center",
    prop: "insTime",
    showOverflowTooltip: true,
  },
  {
    label: "订单状态",
    align: "center",
    prop: "status",
    showOverflowTooltip: true,
  },
  {
    label: "订单来源",
    align: "center",
    prop: "type",
    showOverflowTooltip: true,
  },
  {
    label: "漏单申请的申请时间",
    align: "center",
    prop: "applyOrderTime",
    showOverflowTooltip: true,
  },
  {
    label: "返现金额",
    align: "center",
    prop: "cashbackAmount",
    showOverflowTooltip: true,
  },
];

export const infoTableColumns = [
  {
    label: "渠道名称",
    align: "center",
    prop: "title",
  },
  {
    label: "分享二维码",
    align: "center",
    prop: "linkQrImg",
  },
  {
    label: "分享链接",
    align: "center",
    prop: "linkUrl",
    width: 230,
    showOverflowTooltip: true,
  },
  {
    label: "外部分享链接",
    align: "center",
    prop: "outLinkUrl",
    width: 230,
    showOverflowTooltip: true,
  },

  {
    label: "联系人",
    align: "center",
    prop: "contacter",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "联系人电话",
    align: "center",
    prop: "contacterPhone",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "访客数量",
    align: "center",
    prop: "uvCnt",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "注册数量",
    align: "center",
    prop: "registerCnt",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "认证数量",
    align: "center",
    prop: "identifyCnt",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "订单数",
    align: "center",
    prop: "orderCnt",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "通过订单数",
    align: "center",
    prop: "orderPassedCnt",
    showOverflowTooltip: true,
  },
  // {
  //   label: "备注",
  //   align: "center",
  //   prop: "remarks",
  //   showOverflowTooltip: true,
  // },
];

export const orderTableColumns = [
  {
    label: "渠道名称",
    align: "center",
    prop: "title",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "联系人",
    align: "center",
    prop: "contacter",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "联系人电话",
    align: "center",
    prop: "contacterPhone",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "访客数量",
    align: "center",
    prop: "uvCnt",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "注册数量",
    align: "center",
    prop: "registerCnt",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "订单数",
    align: "center",
    prop: "orderCnt",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "通过订单数",
    align: "center",
    prop: "orderPassedCnt",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "操作",
    align: "center",
    prop: "operation",
    width: 120,
  },
];
