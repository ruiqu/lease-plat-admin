export const tableColumns = [
  {
    label: "姓名",
    align: "center",
    prop: "userName",
    showOverflowTooltip: true,
  },
  {
    label: "提现方式",
    align: "center",
    prop: "type",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "账号",
    align: "center",
    prop: "withdrawalAccount",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "状态",
    align: "center",
    prop: "drawingSts",
    showOverflowTooltip: true,
  },
  {
    label: "时间",
    align: "center",
    prop: "insTime",
    showOverflowTooltip: true,
  },
  {
    label: "金额",
    align: "center",
    prop: "amount",
    showOverflowTooltip: true,
  },
  {
    label: "提现备注",
    align: "center",
    prop: "remarks",
    showOverflowTooltip: true,
  },
  {
    label: "操作",
    align: "center",
    prop: "operation",
    width: 120,
  },
];
