
export const tableColumns = [
  {
    label: "日期",
    align: "center",
    prop: "statDate",
    showOverflowTooltip: true,
  },
  {
    label: "上游uv",
    align: "center",
    prop: "totalChannelUvNumber",
    showOverflowTooltip: true,
  },
  {
    label: "上游注册量",
    align: "center",
    prop: "regTotal",
    showOverflowTooltip: true,
  },
  {
    label: "扣量数",
    align: "center",
    prop: "deductTotal",
    showOverflowTooltip: true,
  },
  {
    label: "结算量",
    align: "center",
    prop: "settleAccounts",
    showOverflowTooltip: true,
  },
  {
    label: "应用总登录数",
    align: "center",
    prop: "loginTotal",
    showOverflowTooltip: true,
  },
  // {
  //   label: "ios登录数",
  //   align: "center",
  //   prop: "title",
  //   showOverflowTooltip: true,
  // },
  // {
  //   label: "安卓登录数",
  //   align: "center",
  //   prop: "title",
  //   showOverflowTooltip: true,
  // },
  // {
  //   label: "小程序登录数",
  //   align: "center",
  //   prop: "title",
  //   showOverflowTooltip: true,
  // }
 
];
export const tableData = [
  {
    title: "标签1",
    id: "1",
    sort: 999,
  },
  {
    title: "标签2",
    id: "2",
    sort: 999,
  },
  {
    title: "标签3",
    id: "3",
    sort: 999,
  },
];
