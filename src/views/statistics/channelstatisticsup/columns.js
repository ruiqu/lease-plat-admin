export const tableColumns = [
  {
    label: "日期",
    align: "center",
    prop: "statDate",
    showOverflowTooltip: true,
    width: 150,
    // type: "index",
  },
  {
    label: "渠道名称",
    align: "center",
    prop: "channelName",
    showOverflowTooltip: true,
    width: 150,
    // type: "index",
  },
  {
    label: "结算方式",
    align: "center",
    prop: "calculateTypeToStr",
    showOverflowTooltip: true,
    width: 150,
    // type: "index",
  },
  {
    label: "负责人",
    align: "center",
    prop: "principal",
    showOverflowTooltip: true,
    width: 150,
    // type: "index",
  },
  {
    label: "渠道单价",
    align: "center",
    prop: "price",
    showOverflowTooltip: true,
    width: 150,
    // type: "index",
  },
  {
    label: "扣量数",
    align: "center",
    prop: "deductNumber",
    showOverflowTooltip: true,
    width: 150,
    // type: "index",
  },
  {
    label: "结算数",
    align: "center",
    prop: "settleAccounts",
    showOverflowTooltip: true,
    width: 150,
    // type: "index",
  },
  {
    label: "uv",
    align: "center",
    prop: "uvNumber",
    showOverflowTooltip: true,
    width: 150,
    // type: "index",
  },
  {
    label: "注册数",
    align: "center",
    prop: "regNumber",
    showOverflowTooltip: true,
    width: 150,
    // type: "index",
  },  
  {
    label: "产品uv",
    align: "center",
    prop: "totalPlatformUvNumber",
    showOverflowTooltip: true,
    width: 150,
    // type: "index",
  },
  {
    label: "产品uv成本",
    align: "center",
    prop: "platformUvCost",
    showOverflowTooltip: true,
    width: 150,
    // type: "index",
  },
  {
    label: "总登陆数",
    align: "center",
    prop: "loginTotal",
    showOverflowTooltip: true,
    width: 150,
    // type: "index",
  },
  {
    label: "ios登陆数",
    align: "center",
    prop: "loginTotalIos",
    showOverflowTooltip: true,
    width: 150,
    // type: "index",
  },
  {
    label: "安卓登陆数",
    align: "center",
    prop: "loginTotalAndroid",
    showOverflowTooltip: true,
    width: 150,
    // type: "index",
  },
  {
    label: "小程序登陆数",
    align: "center",
    prop: "loginTotalApp",
    showOverflowTooltip: true,
    width: 150,
    // type: "index",
  },
  {
    label: "注册率",
    align: "center",
    prop: "regRate",
    showOverflowTooltip: true,
    width: 150,
    // type: "index",
  },

  {
    label: "应用登陆率",
    align: "center",
    prop: "loginRate",
    showOverflowTooltip: true,
    width: 150,
    // type: "index",
  },



  


  

  


  // {
  //   label: "操作",
  //   align: "center",
  //   prop: "operation",
  //   width: 300,
  // },
];
