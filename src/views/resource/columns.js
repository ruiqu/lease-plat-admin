export const resourceTableColumns = [
  {
    label: "资源名称",
    align: "center",
    prop: "name",
    showOverflowTooltip: true,
  },
  {
    label: "资源路径",
    prop: "url",
    align: "center",
    showOverflowTooltip: true,
  },
  {
    label: "资源分类",
    align: "center",
    prop: "categoryName",
    showOverflowTooltip: true,
  },
  {
    label: "资源描述",
    align: "center",
    prop: "description",
    showOverflowTooltip: true,
  },
];
