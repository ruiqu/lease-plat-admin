export const tableColumns = [
  {
    label: "投诉平台",
    align: "center",
    prop: "platformTitle",
    showOverflowTooltip: true,
  },
  {
    label: "投诉内容",
    align: "center",
    prop: "content",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "处理状态",
    align: "center",
    prop: "status",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "用户昵称",
    align: "center",
    prop: "nickName",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "用户姓名",
    align: "center",
    prop: "userName",
    showOverflowTooltip: true,
    // type: "index",
  },
  {
    label: "用户手机号",
    align: "center",
    prop: "phone",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "处理备注",
    align: "center",
    prop: "remarks",
    showOverflowTooltip: true,
    // type: "index",
  },

  {
    label: "操作",
    align: "center",
    prop: "operation",
    width: 100,
  },
];
