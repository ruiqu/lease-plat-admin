import router from "./router";
import store from "./store";
import { Message } from "element-ui";
import NProgress from "nprogress";
import "nprogress/nprogress.css";
import { getToken } from "@/utils/auth";

import Cookies from "js-cookie";
NProgress.configure({ showSpinner: false });

const whiteList = ["/admin/login", "/auth-redirect", "/bind", "/register"];

import { permissionRouters } from "./router";
// console.log(permissionRouters, "-->999");
router.beforeEach((to, from, next) => {
  NProgress.start();
  if (getToken()) {
    /* has token*/
    if (to.path === "/admin/login") {
      next({ path: "/" });
      NProgress.done();
    } else {
      console.log(store.getters.roles, "---store.getters.roles.length--");
      if (store.getters.roles.length === 0 && localStorage.getItem("username")) {
        // 判断当前用户是否已拉取完user_info信息
        store
          .dispatch("GetInfo", localStorage.getItem("username"))
          .then((res) => {
            let cuArr = permissionRouters;
            let { menus } = res.data;
            console.log(menus, "->menus");
            for (let m = 0; m < menus.length; m++) {
              let items = menus[m];
              for (let i = 0; i < cuArr.length; i++) {
                let item = cuArr[i];
                if (item.meta.title == items.title && items.hidden == 0) {
                  item.hidden = false;
                }
                // item.hidden = false;
                for (let j = 0; j < item.children.length; j++) {
                  let v = item.children[j];
                  if (v.meta.title == items.title && items.hidden == 0) {
                    v.hidden = false;
                  }
                  // v.hidden = false;
                }
              }
            }
            console.log(cuArr, "-_>cuArr");
            router.addRoutes(cuArr);
            store.commit("SET_ROUTES", cuArr);
            store.commit("SET_SIDEBAR_ROUTERS", cuArr);
            next({ ...to, replace: true });
          })
          .catch((err) => {
            store.dispatch("LogOut").then(() => {
              store.dispatch("tagsView/delAllViews");
              Message.error(err);
              next({ path: "/" });
            });
          });
      } else {
        next();
      }
    }
  } else {
    // 没有token
    if (whiteList.indexOf(to.path) !== -1) {
      // 在免登录白名单，直接进入
      next();
    } else {
      // next(`/login?redirect=${to.fullPath}`) // 否则全部重定向到登录页
      next(); // 否则全部重定向到登录页
      NProgress.done();
    }
  }
});

router.afterEach(() => {
  NProgress.done();
});
