import Vue from "vue";
import Router from "vue-router";
import { Message } from "element-ui";
import { getToken } from "@/utils/auth";
Vue.use(Router);

/* Layout */
import Layout from "@/layout";
import ParentView from "@/components/ParentView";

/**
 * Note: 路由配置项
 *
 * hidden: true                   // 当设置 true 的时候该路由不会再侧边栏出现 如401，login等页面，或者如一些编辑页面/edit/1
 * alwaysShow: true               // 当你一个路由下面的 children 声明的路由大于1个时，自动会变成嵌套的模式--如组件页面
 *                                // 只有一个时，会将那个子路由当做根路由显示在侧边栏--如引导页面
 *                                // 若你想不管路由下面的 children 声明的个数都显示你的根路由
 *                                // 你可以设置 alwaysShow: true，这样它就会忽略之前定义的规则，一直显示根路由
 * redirect: noRedirect           // 当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
 * name:'router-name'             // 设定路由的名字，一定要填写不然使用<keep-alive>时会出现各种问题
 * meta : {
    noCache: true                // 如果设置为true，则不会被 <keep-alive> 缓存(默认 false)
    title: 'title'               // 设置该路由在侧边栏和面包屑中展示的名字
    icon: 'svg-name'             // 设置该路由的图标，对应路径src/assets/icons/svg
    breadcrumb: false            // 如果设置为false，则不会在breadcrumb面包屑中显示
  }
 */

// 公共路由
export const constantRoutes = [
  {
    path: "/redirect",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "/redirect/:path(.*)",
        component: (resolve) => require(["@/views/redirect"], resolve),
      },
    ],
  },
  {
    path: "/login",
    name: "login",
    component: (resolve) => require(["@/views/login"], resolve),
    hidden: true,
  },
  {
    path: "/404",
    component: (resolve) => require(["@/views/error/404"], resolve),
    hidden: true,
  },
  {
    path: "/401",
    component: (resolve) => require(["@/views/error/401"], resolve),
    hidden: true,
  },
  {
    path: "",
    component: Layout,
    redirect: "index",
    children: [
      {
        path: "index",
        component: (resolve) => require(["@/views/index"], resolve),
        name: "首页",
        meta: { title: "首页", icon: "dashboard", noCache: true, affix: true },
      },
    ],
  },
];
export const permissionRouters = [
  {
    path: "/permission",
    component: Layout,
    hidden: true,
    redirect: "user",
    meta: { title: "权限", icon: "lock" },
    children: [
      {
        hidden: true,
        path: "/user",
        component: (resolve) => require(["@/views/system/user/index"], resolve),
        name: "User",
        meta: { title: "管理员列表", icon: "user" },
      },
      {
        hidden: true,
        path: "/menu",
        component: (resolve) => require(["@/views/menu"], resolve),
        name: "Menu",
        meta: { title: "菜单列表", icon: "tree-table" },
      },
      {
        hidden: true,
        path: "/role",
        component: (resolve) => require(["@/views/rule"], resolve),
        name: "Role",
        meta: { title: "角色列表", icon: "peoples" },
      },
      {
        hidden: true,
        path: "/resourceCategory",
        component: (resolve) => require(["@/views/resourceCategory"], resolve),
        name: "resourceCategory",
        meta: { title: "资源分类", icon: "" },
      },
      {
        hidden: true,
        path: "/resource",
        component: (resolve) => require(["@/views/resource"], resolve),
        name: "resource",
        meta: { title: "资源列表", icon: "component" },
      },
    ],
  },

  {
    path: "/custom",
    component: Layout,
    hidden: true,
    redirect: "customer",
    meta: { title: "用户管理", icon: "system" },
    children: [
      {
        path: "/customer",
        hidden: true,
        component: (resolve) => require(["@/views/customer"], resolve),
        name: "customer",
        meta: { title: "用户管理", icon: "" },
      },
    ],
  },

  {
    path: "/platform",
    component: Layout,
    hidden: true,
    redirect: "platformManage",
    meta: { title: "平台管理", icon: "system" },
    children: [
      {
        path: "/platformManage",
        hidden: true,
        component: (resolve) => require(["@/views/platformManage"], resolve),
        name: "platformManage",
        meta: { title: "平台管理", icon: "" },
      },
    ],
  },
  {
    path: "/platList",
    component: Layout,
    hidden: true,
    redirect: "platListManage",
    meta: { title: "关联平台管理", icon: "system" },
    children: [
      {
        path: "/platListManage",
        hidden: true,
        component: (resolve) => require(["@/views/platformManage/platListManage"], resolve),
        name: "platListManage",
        meta: { title: "关联平台管理", icon: "" },
      },
    ],
  },

  {
    path: "/order",
    component: Layout,
    hidden: true,
    redirect: "orderManage",
    meta: { title: "订单管理", icon: "system" },
    children: [
      {
        path: "/orderManage",
        hidden: true,
        component: (resolve) => require(["@/views/orderManage"], resolve),
        name: "orderManage",
        meta: { title: "订单管理", icon: "" },
      },
    ],
  },
  {
    path: "/classifyManage",
    component: Layout,
    hidden: true,
    redirect: "classify",
    meta: { title: "分类管理", icon: "system" },
    children: [
      {
        path: "/classify",
        hidden: true,
        component: (resolve) => require(["@/views/classifyManage/classify"], resolve),
        name: "classify",
        meta: { title: "分类管理", icon: "" },
      },
      {
        path: "/tabsManage",
        hidden: true,
        component: (resolve) => require(["@/views/classifyManage/tabsManage"], resolve),
        name: "tabsManage",
        meta: { title: "标签Tab管理", icon: "" },
      },
      {
        path: "/entranceManage",
        hidden: true,
        component: (resolve) => require(["@/views/classifyManage/entranceManage"], resolve),
        name: "entranceManage",
        meta: { title: "入口管理", icon: "" },
      },
    ],
  },
  {
    path: "/resourceSetting",
    component: Layout,
    hidden: true,
    redirect: "banner",
    meta: { title: "系统资源设置", icon: "system" },
    children: [
      {
        path: "/tutorialProtocol",
        hidden: true,
        component: (resolve) => require(["@/views/tutorialProtocol"], resolve),
        name: "tutorialProtocol",
        meta: { title: "教程及协议" },
      },
      {
        path: "/banner",
        hidden: true,
        component: (resolve) => require(["@/views/banner"], resolve),
        name: "Banner",
        meta: { title: "Banner设置" },
      },
      {
        path: "/news",
        hidden: true,
        component: (resolve) => require(["@/views/news"], resolve),
        name: "News",
        meta: { title: "头条新闻" },
      },
      {
        path: "/issue",
        hidden: true,
        component: (resolve) => require(["@/views/issueManage/issue"], resolve),
        name: "Issue",
        meta: { title: "常见问题" },
      },

      {
        path: "/issueCategory",
        hidden: true,
        component: (resolve) => require(["@/views/issueManage/issueCategory"], resolve),
        name: "IssueCategory",
        meta: { title: "常见问题分类设置" },
      },
      {
        path: "/setting",
        hidden: true,
        component: (resolve) => require(["@/views/system/setting"], resolve),
        name: "Setting",
        meta: { title: "系统设置", icon: "" },
      },
    ],
  },
  {
    path: "/withdrawal",
    component: Layout,
    hidden: true,
    redirect: "withdrawals",
    meta: { title: "提现管理", icon: "system" },
    children: [
      {
        path: "/withdrawals",
        hidden: true,
        component: (resolve) => require(["@/views/withdrawals"], resolve),
        name: "withdrawals",
        meta: { title: "提现管理", icon: "" },
      },
    ],
  },
  {
    path: "/complaintManage",
    component: Layout,
    hidden: true,
    redirect: "complaint",
    meta: { title: "投诉管理", icon: "system" },
    children: [
      {
        path: "/complaint",
        hidden: true,
        component: (resolve) => require(["@/views/complaintManage"], resolve),
        name: "complaint",
        meta: { title: "投诉管理", icon: "" },
      },
    ],
  },
  {
    path: "/channel",
    component: Layout,
    hidden: true,
    redirect: "channel",
    meta: { title: "渠道管理", icon: "system" },
    children: [
      {
        path: "/channelManage",
        hidden: true,
        component: (resolve) => require(["@/views/channelManage"], resolve),
        name: "channelManage",
        meta: { title: "渠道管理", icon: "" },
      },
      
    ],
  },

  

  {
    path: "/statisticsups",
    component: Layout,
    hidden: true,
    redirect: "statisticsup",
    meta: { title: "上游渠道管理", icon: "system" },
    children: [
      {
        path: "/statisticsup",
        hidden: true,
        component: (resolve) => require(["@/views/statisticsups"], resolve),
        name: "statisticsup",
        meta: { title: "上游渠道管理", icon: "" },
      },
      
   
    ],
  },




  {
    path: "/statistics",
    component: Layout,
    hidden: true,
    redirect: "channelstatisticsup",
    meta: { title: "渠道统计", icon: "system" },
    children: [
      {
        path: "/channelstatisticsup",
        hidden: true,
        component: (resolve) => require(["@/views/statistics/channelstatisticsup"], resolve),
        name: "channelstatisticsup",
        meta: { title: "上游渠道统计", icon: "" },
      },
      {
        path: "/channelstatisticsproduct",
        hidden: true,
        component: (resolve) => require(["@/views/statistics/channelstatisticsproduct"], resolve),
        name: "channelstatisticsproduct",
        meta: { title: "产品渠道", icon: "" },
      },
      {
        path: "/channelstatisticstotal",
        hidden: true,
        component: (resolve) => require(["@/views/statistics/channelstatisticstotal"], resolve),
        name: "channelstatisticstotal",
        meta: { title: "总统计", icon: "" },
      },
   
    ],
  },
  {
    path: "/channelInfo",
    component: Layout,
    hidden: true,
    redirect: "channel",
    meta: { title: "统计数据", icon: "" },
    children: [
      {
        path: "/channelInfo",
        hidden: true,
        component: (resolve) => require(["@/views/channelManage/channelInfo"], resolve),
        name: "channelInfo",
        meta: { title: "统计数据", icon: "" },
      },
    ],
  },
 

];
let router = new Router({
  // mode: "history", // 去掉url中的#
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes,
});
router.beforeEach((to, from, next) => {
  if (!getToken() || !localStorage.getItem("username")) {
    if (to.path == "/login") {
      //如果是登录页面路径，就直接next()
      next();
    } else {
      Message("暂无登录信息");
      //不然就跳转到登录；
      next("/login");
    }
    return;
  }
  next();
});
export default router;
