import {
  bannerList,
  createBanner,
  bannerUpdateById,
  bannerInfoById,
  deleteByIdBanner,
} from "@/api/banner";

import Cookies from "js-cookie";
const user = {
  state: {},

  mutations: {},

  actions: {
    // 登录
    BannerList({ commit }, data) {
      return new Promise((resolve, reject) => {
        bannerList(data)
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    CreateBanner({ commit }, data) {
      return new Promise((resolve, reject) => {
        createBanner(data)
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    BannerUpdateById({ commit }, data) {
      return new Promise((resolve, reject) => {
        bannerUpdateById(data)
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    BannerInfoById({ commit }, data) {
      return new Promise((resolve, reject) => {
        bannerInfoById(data)
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    DeleteByIdBanner({ commit }, data) {
      return new Promise((resolve, reject) => {
        deleteByIdBanner(data)
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
  },
};

export default user;
