import {
  resourceCategoryCreate,
  resourceCategoryListAll,
  resourceCategoryUpdateById,
  resourceCategoryDeleteById,
} from "@/api/resourceCategory";
// import { drugCategoryWithChildren } from '@/utils/auth'

import Cookies from "js-cookie";
const user = {
  state: {},

  mutations: {},

  actions: {
    // 登录
    ResourceCategoryCreate({ commit }, data) {
      return new Promise((resolve, reject) => {
        resourceCategoryCreate(data)
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    ResourceCategoryListAll({ commit }, data) {
      return new Promise((resolve, reject) => {
        resourceCategoryListAll(data)
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    ResourceCategoryUpdateById({ commit }, data) {
      return new Promise((resolve, reject) => {
        resourceCategoryUpdateById(data)
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    ResourceCategoryDeleteById({ commit }, data) {
      return new Promise((resolve, reject) => {
        resourceCategoryDeleteById(data)
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
  },
};

export default user;
