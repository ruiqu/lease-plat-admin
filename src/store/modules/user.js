import { login, logout, getInfo } from "@/api/login";
import { getToken, setToken, removeToken } from "@/utils/auth";

import Cookies from "js-cookie";
const user = {
  state: {
    token: getToken(),
    name: "",
    avatar: "",
    roles: [],
    permissions: [],
    userId: "",
    userInfo: {},
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token;
    },
    SET_NAME: (state, name) => {
      state.name = name;
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar;
    },
    SET_ROLES: (state, roles) => {
      console.log(roles, "----->rolesrolesrolesrolesroles");
      state.roles = roles;
    },
    SET_PERMISSIONS: (state, permissions) => {
      state.permissions = permissions;
    },
    SET_USERID: (state, userId) => {
      state.userId = userId;
    },
    SET_USERINFO: (state, userInfo) => {
      state.userInfo = userInfo;
    },
  },

  actions: {
    // 登录
    Login({ commit }, userInfo) {
      const username = userInfo.username.trim();
      const password = userInfo.password;
      return new Promise((resolve, reject) => {
        login(username, password)
          .then((res) => {
            // console.log(res);
            let _string = res.data.token;
            // console.log(res);
            setToken(_string);
            commit("SET_TOKEN", _string);

            resolve();
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    // 获取用户信息
    GetInfo({ commit, state }, name) {
      // console.log(state, "-->0000");
      return new Promise((resolve, reject) => {
        let username = name || Cookies.get("username");
        getInfo(username)
          .then((res) => {
            if (typeof res == "undefined") {
              return;
            }
            console.log(res, "------userinfo--------");
            const user = res.data;

            commit("SET_USERID", res.data.id);
            commit("SET_USERINFO", res.data);
            if (user.menus && user.menus.length > 0) {
              // 验证返回的roles是否是一个非空数组
              commit("SET_ROLES", user.roles);
              // commit('SET_PERMISSIONS', res.permissions)
            } else {
              commit("SET_ROLES", ["ROLE_DEFAULT"]);
            }
            commit("SET_NAME", user.userName);
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    // 退出系统
    LogOut({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout(state.token)
          .then(() => {
            commit("SET_TOKEN", "");
            commit("SET_ROLES", []);
            commit("SET_PERMISSIONS", []);
            commit("REST_ROUTER");
            window.localStorage.clear();
            resolve();
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    // 前端 登出
    FedLogOut({ commit }) {
      return new Promise((resolve) => {
        commit("SET_TOKEN", "");
        resolve();
      });
    },
  },
};

export default user;
