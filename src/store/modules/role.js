import {
  ruleList,
  roleCreate,
  roleUpdateById,
  roleUpdateStatusById,
  roleDelete,
  roleAllocMenuByRoleId,
  roleListAll,
  roleListMenuByRoleId,
  listResourceByRoleId,
  allocResourceByRoleId
} from "@/api/rule";
// import { drugCategoryWithChildren } from '@/utils/auth'

import Cookies from "js-cookie";
const user = {
  state: {},

  mutations: {},

  actions: {
    // 登录
    RuleList({ commit }, data) {
      return new Promise((resolve, reject) => {
        ruleList(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    RoleCreate({ commit }, data) {
      return new Promise((resolve, reject) => {
        roleCreate(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    RoleUpdateById({ commit }, data) {
      return new Promise((resolve, reject) => {
        roleUpdateById(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    RoleUpdateStatusById({ commit }, data) {
      return new Promise((resolve, reject) => {
        roleUpdateStatusById(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    RoleDelete({ commit }, data) {
      return new Promise((resolve, reject) => {
        roleDelete(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    RoleAllocMenuByRoleId({ commit }, data) {
      return new Promise((resolve, reject) => {
        roleAllocMenuByRoleId(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    RoleListAll({ commit }, data) {
      return new Promise((resolve, reject) => {
        roleListAll(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    RoleListMenuByRoleId({ commit }, data) {
      return new Promise((resolve, reject) => {
        roleListMenuByRoleId(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    ListResourceByRoleId({ commit }, data) {
      return new Promise((resolve, reject) => {
        listResourceByRoleId(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    AllocResourceByRoleId({ commit }, data) {
      return new Promise((resolve, reject) => {
        allocResourceByRoleId(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    }
  }
};

export default user;
