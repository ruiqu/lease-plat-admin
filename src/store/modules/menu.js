import {
  menuCreate,
  menuDeleteById,
  menuUpdate,
  menuUpdateHiddenById,
  menuInfoById,
  menuListByParentId
} from "@/api/menu";
// import { drugCategoryWithChildren } from '@/utils/auth'

import Cookies from "js-cookie";
const user = {
  state: {},

  mutations: {},

  actions: {
    // 登录
    MenuCreate({ commit }, data) {
      return new Promise((resolve, reject) => {
        menuCreate(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    // 登录
    MenuCreate({ commit }, data) {
      return new Promise((resolve, reject) => {
        menuCreate(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    MenuDeleteById({ commit }, data) {
      return new Promise((resolve, reject) => {
        menuDeleteById(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    MenuUpdate({ commit }, data) {
      return new Promise((resolve, reject) => {
        menuUpdate(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    MenuUpdateHiddenById({ commit }, data) {
      return new Promise((resolve, reject) => {
        menuUpdateHiddenById(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },

    MenuInfoById({ commit }, data) {
      return new Promise((resolve, reject) => {
        menuInfoById(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    MenuListByParentId({ commit }, data) {
      return new Promise((resolve, reject) => {
        menuListByParentId(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    }
  }
};

export default user;
