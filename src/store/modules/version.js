import { versionInfo, versionUpdateById } from "@/api/version";
// import { drugCategoryWithChildren } from '@/utils/auth'

import Cookies from "js-cookie";
const user = {
  state: {},

  mutations: {},

  actions: {
    // 登录
    VersionInfo({ commit }, data) {
      return new Promise((resolve, reject) => {
        versionInfo(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    VersionUpdateById({ commit }, data) {
      return new Promise((resolve, reject) => {
        versionUpdateById(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    }
  }
};

export default user;
