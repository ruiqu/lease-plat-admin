import {
  adminList,
  adminRegister,
  adminInfoById,
  adminUpdateById,
  adminDeleteById,
  adminUpdateStatusById,
  adminRoleUpdate,
  addminRoleById
} from "@/api/admin";
// import { drugCategoryWithChildren } from '@/utils/auth'

import Cookies from "js-cookie";
const user = {
  state: {},

  mutations: {},

  actions: {
    // 登录
    AdminList({ commit }, data) {
      return new Promise((resolve, reject) => {
        adminList(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    AdminRegister({ commit }, data) {
      return new Promise((resolve, reject) => {
        adminRegister(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    AdminInfoById({ commit }, data) {
      return new Promise((resolve, reject) => {
        adminInfoById(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    AdminUpdateById({ commit }, data) {
      return new Promise((resolve, reject) => {
        adminUpdateById(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    AdminDeleteById({ commit }, data) {
      return new Promise((resolve, reject) => {
        adminDeleteById(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    AdminUpdateStatusById({ commit }, data) {
      return new Promise((resolve, reject) => {
        adminUpdateStatusById(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    AdminRoleUpdate({ commit }, data) {
      return new Promise((resolve, reject) => {
        adminRoleUpdate(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    },
    AddminRoleById({ commit }, data) {
      return new Promise((resolve, reject) => {
        addminRoleById(data)
          .then(res => {
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    }
  }
};

export default user;
