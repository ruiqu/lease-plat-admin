import { resourceCreate, resourceListAll, resourceUpdateById, resourceList, resourceDeleteById, resourceInfoById } from "@/api/resource";
// import { drugCategoryWithChildren } from '@/utils/auth'

import Cookies from "js-cookie";
const user = {
  state: {},

  mutations: {},

  actions: {
    // 登录
    ResourceCreate({ commit }, data) {
      return new Promise((resolve, reject) => {
        resourceCreate(data)
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    ResourceListAll({ commit }, data) {
      return new Promise((resolve, reject) => {
        resourceListAll(data)
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },

    ResourceList({ commit }, data) {
      return new Promise((resolve, reject) => {
        resourceList(data)
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    ResourceUpdateById({ commit }, data) {
      return new Promise((resolve, reject) => {
        resourceUpdateById(data)
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    ResourceDeleteById({ commit }, data) {
      return new Promise((resolve, reject) => {
        resourceDeleteById(data)
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
    ResourceInfoById({ commit }, data) {
      return new Promise((resolve, reject) => {
        resourceInfoById(data)
          .then((res) => {
            resolve(res);
          })
          .catch((error) => {
            reject(error);
          });
      });
    },
  },
};

export default user;
