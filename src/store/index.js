import Vue from "vue";
import Vuex from "vuex";
import app from "./modules/app";
import user from "./modules/user";
import tagsView from "./modules/tagsView";
import permission from "./modules/permission";
import settings from "./modules/settings";
import banner from "./modules/banner";
import admin from "./modules/admin";
import role from "./modules/role";
import menu from "./modules/menu";
import resourceCategory from "./modules/resourceCategory";
import resource from "./modules/resource";
import version from "./modules/version";

import getters from "./getters";

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    app,
    user,
    tagsView,
    permission,
    settings,
    banner,
    admin,
    role,
    menu,
    resourceCategory,
    resource,
    version,
  },
  getters,
});

export default store;
